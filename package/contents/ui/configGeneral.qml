/*
 * Copyright 2015 Kai Uwe Broulik <kde@privat.broulik.de>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

import QtQuick 2.0
import QtQuick.Controls 1.0 as QtControls
import QtQuick.Layouts 1.0
import QtQuick.Dialogs 1.2

ColumnLayout {
    id: appearancePage

    property bool cfg_showFace
    property bool cfg_showName
    property bool cfg_showFullName
    property bool cfg_showSett
    property alias cfg_icon: icon.text
    property alias cfg_showTechnicalInfo: showTechnicalInfoCheck.checked
    property alias cfg_windowsMenuentry: windowsMenuentry.text

    QtControls.GroupBox {
        title: i18n("User name display")
        flat: true

        QtControls.ExclusiveGroup {
            id: nameEg
            onCurrentChanged: cfg_showFullName = (current === showFullNameRadio)
        }

        ColumnLayout {
            QtControls.RadioButton {
                id: showFullNameRadio
                Layout.fillWidth: true
                exclusiveGroup: nameEg
                text: i18n("Show full name (if available)")
                checked: cfg_showFullName
            }

            QtControls.RadioButton {
                Layout.fillWidth: true
                exclusiveGroup: nameEg
                text: i18n("Show login username")
                checked: !cfg_showFullName
            }
        }
    }

    QtControls.GroupBox {
        title: i18n("Layout")
        flat: true

        QtControls.ExclusiveGroup {
            id: layoutEg
            onCurrentChanged: {
                cfg_showSett = (current === showOnlySettRadio)
                cfg_showName = (current === showOnlyNameRadio || current === showBothRadio)
                cfg_showFace = (current === showOnlyFaceRadio || current === showBothRadio)
                cfg_settIcon = setIcon.text
            }
        }

        ColumnLayout {
            QtControls.RadioButton {
                id: showOnlyNameRadio
                Layout.fillWidth: true
                exclusiveGroup: layoutEg
                text: i18n("Show only name")
                checked: cfg_showName && !cfg_showFace && !cfg_showSett
            }

            QtControls.RadioButton {
                id: showOnlyFaceRadio
                Layout.fillWidth: true
                exclusiveGroup: layoutEg
                text: i18n("Show only avatar")
                checked: !cfg_showName && cfg_showFace && !cfg_showSett
            }

            QtControls.RadioButton {
                id: showOnlySettRadio
                Layout.fillWidth: true
                exclusiveGroup: layoutEg
                text: i18n("Show only icon")
                checked: !cfg_showName && !cfg_showFace && cfg_showSett
            }

            QtControls.RadioButton {
                id: showBothRadio
                Layout.fillWidth: true
                exclusiveGroup: layoutEg
                text: i18n("Show both avatar and name")
                checked: cfg_showName && cfg_showFace && !cfg_showSett
            }

            QtControls.CheckBox {
                id: showTechnicalInfoCheck
                text: i18n("Show technical information about sessions")
            }

            QtControls.Label {
                        text: i18n('Icon:')
                    }

            RowLayout {
                QtControls.TextField {
                    id: icon
                    implicitWidth: 300
                }

                QtControls.Button {
                    iconName: 'folder'
                    onClicked: {
                        iconDialog.open()
                    }
                }
            }

            QtControls.Label {
                text: i18n("\nHow to enable restart to Windows:")
            }
            RowLayout {
                QtControls.Label {
                    text: i18n("1. Click right button to save the script")
                }
                QtControls.Button {
                    iconName: 'document-save'
                    onClicked: saveDialog.open()
                }
            }
            QtControls.Label {
                text: i18n("2. Here is the path of the saved script, do not remove this script")
            }
            QtControls.TextField {
                id: windowsMenuentry
                implicitWidth: 400
            }
            QtControls.Label {
                text: i18n("3. click apply, and read the comments in the script to set sudo permission")
            }
        }
    }

    FileDialog {
        id: iconDialog
        title: 'Please choose an image file'
        folder: '/usr/share/icons/breeze/'
        nameFilters: ['Image files (*.png *.jpg *.xpm *.svg *.svgz)', 'All files (*)']
        onAccepted: {
            icon.text = iconDialog.fileUrl
        }
    }

    function saveFile(fileUrl, text) {
        var request = new XMLHttpRequest();
        request.open("PUT", fileUrl, false);
        request.send(text);
        return request.status;
    }

    FileDialog {
        id: saveDialog
        selectExisting: false
        nameFilters: ["Bash script (*.sh)"]
        property string restartScript: '#!/bin/bash\n'
            + '# use grub-reboot to temporarily reboot to Windows only in the next reboot.\n'
            + '# Before using this script, you need to add this file to sudoers,\n'
            + '# suppose the path of this file is "/opt/restart_to_windows.sh"\n'
            + '#   sudo chown root:root /opt/restart_to_windows.sh\n'
            + '#   sudo chmod 755 /opt/restart_to_windows.sh\n'
            + '#   sudo echo "$USER ALL=(ALL) NOPASSWD: /opt/reboot-into-windows" > /etc/sudoers.d/50-windows-reboot\n'
            + '# Use this script as\n'
            + '#   sudo /opt/restart_to_windows.sh\n'
            + '# Or you can create another wrapper script, e.g. "restart_to_windows_wrapper.sh"\n'
            + '# with the following contents\n'
            + '#   #!/bin/bash\n'
            + '#   sudo /opt/restart_to_windows.sh\n'
            + '# and run it just by `./restart_to_windows_wrapper.sh` without sudo prefix.\n\n'
            + 'entry=`grep -E "^menuentry" /boot/grub/grub.cfg | grep -n Windows | cut -d ":" -f 1`\n\n'
            + 'grub-reboot $entry\n'
            + '# you might need to tweak a bit the number\n'
            + '#grub-reboot $(($entry - 1))\n\n'
            + 'reboot\n'
        onAccepted: {
            saveFile(saveDialog.fileUrl, restartScript)
            // string prefix: file://
            windowsMenuentry.text = saveDialog.fileUrl.toString().substring(7)
        }
    }

    Item { // tighten layout
        Layout.fillHeight: true
    }
}
